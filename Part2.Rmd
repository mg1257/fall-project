---
title: "Part2"
author: "Monit Guin"
date: '2018-11-05'
output: html_document
runtime: shiny
resource_files:
- consumer_complaints.csv
---


```{r step1_data_cleansing} 
### Please change working direcory to consumer_complaints.csv ###
library(shiny)
library(dplyr)
library(ggplot2)
library(plotly)
library(lubridate)
library(readr)
library(Hmisc)

### Please change working direcory for consumer_complaints.csv ###

bankdf<-read_csv(file="/Users/Monit/Documents/UNH/DATA 901/R Programming 2/Project/Part 2/us-consumer-finance-complaint-database/consumer_complaints.csv")

### Step 1: Cleaning the dataset, by dropping columns. ###

bankdfclean <- bankdf[-c(6,7,11,12) ]
bankdfcleanna<-na.omit(bankdfclean)

## Removing rows that has sub_product with "I do not know" ##
bankdfcleanna<-bankdfcleanna%>%
  filter(sub_product!="I do not know")

```

```{r UI_frameowrk}
library(hflights)
library(shiny)
library(dplyr)
library(ggplot2)
library(plotly)
library(lubridate)

### Creating new column for Day of Week ###

class(bankdfcleanna$date_sent_to_company)
bankdfcleanna$date_sent_to_company<-as.Date(bankdfcleanna$date_sent_to_company,format = "%m/%d/%y")
bankdfcleanna$DayOfWeek<-wday(bankdfcleanna$date_sent_to_company,label = TRUE)


### Creating Tab Panels for Products, Communication Methods & Day of Week ###


ui <- fluidPage(
  sidebarLayout(
    sidebarPanel(
      tabsetPanel(
        tabPanel("Products",
                  selectInput("sub","Select Product", choices=unique(bankdfcleanna$sub_product), selected = NULL, multiple = FALSE, selectize = TRUE, width = NULL, size = NULL)),
        tabPanel("Communication Methods",
                 radioButtons("rb", "Select ", choices = unique(bankdfcleanna$sub_product), selected = unique(bankdfcleanna$sub_product)[1],inline = FALSE),
                 selectInput("si","Submitted By",choices = unique(bankdfcleanna$submitted_via))
                 
        ),

    tabPanel("Day of Week",
                 checkboxGroupInput("si1","Day Of Week",choices = unique(bankdfcleanna$DayOfWeek), selected = NULL)),
    
    submitButton("Refresh", icon("refresh"))
      )
 
### Creating Main Panels for Products, Communication Methods & Day of Week ###     
    ),
    mainPanel(
      tabsetPanel(
        tabPanel("Products",
                 h3("Product complaints by product type"),
                 plotlyOutput("bar1")
        ),
        tabPanel("Communication Methods",
                 h3("Product complaints filtered by product type and communication methods"),
                 plotlyOutput("bar2")
        ),   
        tabPanel("Day of Week",
                 h3("Filter table by day of week"),
                 dataTableOutput(outputId = "table")
        ),
        tabPanel("Read Me",h4("Introduction"),
                p("Consumer Financial Protection Bureau (CFPB) was created to  protect consumers from unfair, deceptive, or abusive practices and take action against companies that break the law."),h4("About the Dataset"),p("Each week the CFPB sends thousands of consumers’ complaints about financial products and services to companies for response. Those complaints are published here after the company responds or after 15 days, whichever comes first. By adding their voice, consumers help improve the financial marketplace. This dataset is from 2013 - 2016. "), h4("Insights"),p("Products - Displays number of complaints of top 5 Financial Institutions by their respective products."), p("Communication Methods - Displays number of complaints of top 5 Financial Institutions by their respective products and communication method used."), p("Day of Week - Look up various records based on combination days of the week.")
                )
      
      )
      
    )
  )
  
  
)



server <- function(input, output,session) {
### Creating filter  for Products ###   
  
        filtered_data1 <- reactive({filter(bankdfcleanna, sub_product == input$sub)})
        filtered_data1_1 <- reactive({filtered_data1() %>%
  group_by(company)%>%
  summarise(n=n())%>%
  arrange(desc(n))%>%
  head(5)})

### Creating filter Communication Methods ###  
filtered_data2 <- reactive({filter(bankdfcleanna, sub_product == input$rb)})
 
filtered_data2_1 <- reactive({filter(filtered_data2(), submitted_via == input$si)})    

 
   filtered_data2_2 <- reactive({filtered_data2_1() %>%
  group_by(company)%>%
  summarise(n=n())%>%
  arrange(desc(n))%>%
  head(5)})
  
### Creating filter for Day of Week ###
                        
filtered_data3_1 <- reactive({filter(bankdfcleanna, DayOfWeek == input$si1)})    

### Creating graph  for Products ###                    
        
output$bar1 <- renderPlotly(
                {
                         
  p <- ggplot(filtered_data1_1(), aes(x =company,y=n,fill=company))+geom_histogram(stat= "identity",position = "dodge") + theme(axis.text.x = element_text(angle = 90,size=7, vjust = 0.5)) + xlab("Financial Institutions") + ylab("Number of Complaints")
                    
   ggplotly(p)   
                                            
  
}
)

### Creating graph  for Communication Methods ###                    
        

output$bar2 <- renderPlotly(
                {
                         
  p <- ggplot(filtered_data2_2(), aes(x =company,y=n,fill=company))+geom_histogram(stat= "identity") + theme(axis.text.x = element_text(angle = 90,size = 7, vjust = 0.5)) + xlab("Financial Institutions") + ylab("Number of Complaints") 
                    
   ggplotly(p)   
                                            
  
}
)

### Creating table  to filter Day of Week ###  
output$table <- renderDataTable(
    {
      select(filtered_data3_1(),c("company","sub_product","issue","state","submitted_via","timely_response","DayOfWeek"))
    }    
)                                           
  
}

shinyApp(ui, server)
```